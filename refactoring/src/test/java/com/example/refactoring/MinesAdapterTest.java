package com.example.refactoring;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MinesAdapterTest {

    private Board.MinesAdapter minesAdapter;

    @BeforeEach
    void setup() {
        Board board = new Board(new JLabel());
        minesAdapter = spy(board.new MinesAdapter());
    }

    @ParameterizedTest
    @MethodSource("provideMouseEvents")
    void testMousePressed(MouseEvent event) {
        assertDoesNotThrow(() -> minesAdapter.mousePressed(event));
    }

    private static Stream<MouseEvent> provideMouseEvents() {
        List<MouseEvent> events = Arrays.asList(
                createMouseEvent(-1, -1, MouseEvent.BUTTON1),
                createMouseEvent(1000, 1000, MouseEvent.BUTTON3),
                createMouseEvent(2, 10, MouseEvent.BUTTON3),
                createMouseEvent(3, 10, MouseEvent.BUTTON3),
                createMouseEvent(4, 10, MouseEvent.BUTTON3),
                createMouseEvent(5, 10, MouseEvent.BUTTON3),
                createMouseEvent(6, 10, MouseEvent.BUTTON3),
                createMouseEvent(7, 10, MouseEvent.BUTTON3),
                createMouseEvent(8, 10, MouseEvent.BUTTON3),
                createMouseEvent(9, 10, MouseEvent.BUTTON3)
        );

        return events.stream();
    }

    private static MouseEvent createMouseEvent(int x, int y, int button) {
        return new MouseEvent(new JPanel(), MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, x, y, 1, false, button);
    }
}

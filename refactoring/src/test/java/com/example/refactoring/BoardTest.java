package com.example.refactoring;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.JLabel;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BoardTest {

    private static final int NUMBER_OF_MINES = 40;

    private static final int NUMBER_OF_IMAGES = 256;
    @Mock
    private JLabel statusbar;

    @Mock
    private Graphics graphics;

    @Test
    void paintComponent() {
        Board board = new Board(statusbar);
        board.paintComponent(graphics);

        verify(graphics, times(NUMBER_OF_IMAGES))
				.drawImage(any(Image.class), anyInt(), anyInt(), any(ImageObserver.class));
        verify(statusbar).setText(String.valueOf(NUMBER_OF_MINES));
        verifyNoMoreInteractions();
    }

    @Test
    void board() throws IllegalAccessException, NoSuchFieldException {
        Board board = new Board(statusbar);

        Field minesLeftField = Board.class.getDeclaredField("minesLeft");
        minesLeftField.setAccessible(true);
        int minesLeft = (int) minesLeftField.get(board);

        Field inGameField = Board.class.getDeclaredField("inGame");
        inGameField.setAccessible(true);
        boolean inGame = (boolean) inGameField.get(board);

        assertEquals(NUMBER_OF_MINES, minesLeft);
        assertTrue(inGame);

        verify(statusbar).setText(String.valueOf(NUMBER_OF_MINES));
        verifyNoMoreInteractions();
    }

	private void verifyNoMoreInteractions() {
		Mockito.verifyNoMoreInteractions(statusbar, graphics);
	}

}
